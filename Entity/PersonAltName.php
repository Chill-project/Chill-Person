<?php

namespace Chill\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonAltName
 *
 * @ORM\Table(name="person_alt_name")
 * @ORM\Entity(repositoryClass="Chill\PersonBundle\Repository\PersonAltNameRepository")
 */
class PersonAltName
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=255)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="text")
     */
    private $label;
    
    /**
     *
     * @var Person
     * @ORM\OneToMany(
     *  targetEntity="Chill\PersonBundle\Entity\Person",
     *  mappedBy="altNames"
     * )
     */
    private $person;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key.
     *
     * @param string $key
     *
     * @return PersonAltName
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return PersonAltName
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person = null)
    {
        $this->person = $person;
        
        return $this;
    }
}
