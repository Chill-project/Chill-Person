<?php

/*
 * 
 * Copyright (C) 2014-2020, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Entity\AccompanyingPeriod;

use Doctrine\Common\Collections\Collection;

/**
 * ClosingMotive give an explanation why we closed the Accompanying period
 */
class ClosingMotive
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $name;
    
    /**
     *
     * @var boolean
     */
    private $active = true;
    
    /**
     *
     * @var self
     */
    private $parent = null;
    
    /**
     * child Accompanying periods
     *
     * @var Collection
     */
    private $children;
    
    /**
     *
     * @var float
     */
    private $ordering = 0.0;
    
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param array $name
     *
     * @return ClosingMotive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active)
    {
        $this->active = $active;
        
        if ($this->active === FALSE) {
            foreach ($this->getChildren() as $child) {
                $child->setActive(FALSE);
            }
        }
        
        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setParent(?ClosingMotive $parent): ClosingMotive
    {
        $this->parent = $parent;
        
        if (NULL !== $parent) {
            //$parent->addChildren($this);
        }
        
        return $this;
    }

    public function setChildren(Collection $children): ClosingMotive
    {
        $this->children = $children;
        
        return $this;
    }
    
    public function addChildren(ClosingMotive $child): ClosingMotive
    {
        if ($this->children->contains($child)) {
            return $this;
        }
        
        $this->children->add($child);
        $child->setParent($this);
        
        return $this;
    }
    
    public function removeChildren(ClosingMotive $child): ClosingMotive
    {
        if ($this->children->removeElement($child)) {
            $child->setParent(null);
        }
        
        return $this;
    }
    
    public function getOrdering(): float
    {
        return $this->ordering;
    }

    public function setOrdering(float $ordering)
    {
        $this->ordering = $ordering;
        
        return $this;
    }
        
    public function isChild(): bool
    {
        return $this->parent !== null;
    }
    
    public function isParent(): bool
    {
        return $this->children->count() > 0;
    }
    
    public function isLeaf(): bool
    {
        return $this->children->count() === 0;
    }
    
    public function hasParent(): bool
    {
        return $this->parent !== null;
    }

}

