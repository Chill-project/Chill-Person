<?php

namespace Chill\PersonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Chill\PersonBundle\Widget\PersonListWidgetFactory;
use Chill\PersonBundle\DependencyInjection\CompilerPass\AccompanyingPeriodTimelineCompilerPass;

class ChillPersonBundle extends Bundle
{
    public function build(ContainerBuilder $container) 
    {
        parent::build($container);
        
        $container->getExtension('chill_main')
            ->addWidgetFactory(new PersonListWidgetFactory());
        
        $container->addCompilerPass(new AccompanyingPeriodTimelineCompilerPass());
    }
}
