<?php

namespace Chill\PersonBundle\Privacy;

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2015, Champs Libres Cooperative SCRLFS,
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Symfony\Component\EventDispatcher\Event;
use Chill\PersonBundle\Entity\Person;

/**
 * Class PrivacyEvent
 *
 * Array $args expects arguments with the following keys: 'element_class', 'element_id', 'action'
 * By default, action is set to 'show'
 *
 * @package Chill\PersonBundle\Privacy
 */
class PrivacyEvent extends Event
{
    const PERSON_PRIVACY_EVENT = 'chill_person.privacy_event';
    
    /**
     * @var Person
     */
    private $person;
    
    /**
     * @var array
     */
    private $args;
    
    /**
     * @var array
     */
    private $persons;
    
    /**
     * PrivacyEvent constructor.
     *
     * @param Person $person
     * @param array $args
     */
    public function __construct(Person $person, array $args = array('action' => 'show'))
    {
        $this->person = $person;
        $this->args = $args;
        $this->persons = array();
    }
    
    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }
    
    /**
     * @param Person $person
     */
    public function addPerson(Person $person)
    {
        $this->persons[] = $person;
    
        return $this;
    }
    
    /**
     * @return array $persons
     */
    public function getPersons()
    {
        return $this->persons;
    }
    
    /**
     * @return bool
     */
    public function hasPersons()
    {
        return count($this->persons) >= 1;
    }
    
    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }
    
}