<?php

namespace Chill\PersonBundle\Privacy;

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2015, Champs Libres Cooperative SCRLFS,
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\PersonBundle\Entity\Person;

class PrivacyEventSubscriber implements EventSubscriberInterface
{
    
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var TokenStorageInterface
     */
    protected $token;
    
    /**
     * PrivacyEventSubscriber constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, TokenStorageInterface $token)
    {
        $this->logger = $logger;
        $this->token = $token;
    }
    
    public static function getSubscribedEvents()
    {
        return array(PrivacyEvent::PERSON_PRIVACY_EVENT => array(
            array('onPrivacyEvent')
        ));
    }
    
    public function onPrivacyEvent(PrivacyEvent $event)
    {
        $persons = array();
        
        if ($event->hasPersons() === true) {
            foreach ($event->getPersons() as $person) {
                $persons[] = $person->getId();
            }
        }

        $involved = array(
            'by_user' => $this->token->getToken()->getUser()->getUsername(),
            'by_user_id' => $this->token->getToken()->getUser()->getId(),
            'person_id' => $event->getPerson()->getId(),
        );
        
        if ($event->hasPersons()) {
            $involved['persons'] = \array_map(
                function(Person $p) { return $p->getId(); }, 
                $event->getPersons()
            );
        }
        
        $this->logger->notice(
            "[Privacy Event] A Person Folder has been viewed",
            array_merge($involved, $event->getArgs())
        );
    }
}