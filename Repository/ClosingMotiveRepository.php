<?php
/*
 * Copyright (C) 2020 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Entity repository for closing motives
 *
 */
class ClosingMotiveRepository extends \Doctrine\ORM\EntityRepository
{
    public function getActiveClosingMotive(bool $onlyLeaf = true)
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata($this->getClassName(), 'cm');
        
        $sql = "SELECT ".(string) $rsm."
            FROM chill_person_closingmotive AS cm
            WHERE
                active IS TRUE ";
        
        if ($onlyLeaf) {
            $sql .= "AND cm.id NOT IN (
                SELECT DISTINCT parent_id FROM chill_person_closingmotive WHERE parent_id IS NOT NULL
            )";
        }
        
        $sql .= " ORDER BY cm.ordering ASC";
        
        return $this->_em
            ->createNativeQuery($sql, $rsm)
            ->getResult()
            ;
    }
}
