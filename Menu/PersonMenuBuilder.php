<?php
/*
 * Copyright (C) 2018 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Add menu entrie to person menu.
 * 
 * Menu entries added :
 * 
 * - person details ;
 * - accompanying period (if `visible`)
 *
 */
class PersonMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var string 'visible' or 'hidden'
     */
    protected $showAccompanyingPeriod;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(
        $showAccompanyingPeriod,
        TranslatorInterface $translator
    ) {
        $this->showAccompanyingPeriod = $showAccompanyingPeriod;
        $this->translator = $translator;
    }
    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        $menu->addChild($this->translator->trans('Person details'), [
                'route' => 'chill_person_view',
                'routeParameters' => [
                    'person_id' => $parameters['person']->getId()
                ]
            ])
            ->setExtras([
                'order' => 50
            ]);
        
        if ($this->showAccompanyingPeriod === 'visible') {
            $menu->addChild($this->translator->trans('Accompanying period list'), [
                    'route' => 'chill_person_accompanying_period_list',
                    'routeParameters' => [
                        'person_id' => $parameters['person']->getId()
                    ]
                ])
                ->setExtras([
                    'order' => 100
                ]);
        }
    }

    public static function getMenuIds(): array
    {
        return [ 'person' ];
    }
}
