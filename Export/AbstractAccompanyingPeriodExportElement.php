<?php
/*
 * Copyright (C) 2019 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Export;

use Doctrine\ORM\QueryBuilder;

/**
 * 
 *
 */
class AbstractAccompanyingPeriodExportElement
{
    /**
     * Return true if "accompanying_period" alias is present in the query alises.
     * 
     * @param QueryBuilder $query
     * @return bool
     */
    protected function havingAccompanyingPeriodInJoin(QueryBuilder $query): bool
    {
        $joins = $query->getDQLPart('join') ?? [];
        
        return (\in_array('accompanying_period', $query->getAllAliases()));
    }
    
    /**
     * Add the accompanying period alias to the query
     * 
     * @param QueryBuilder $query
     * @return void
     * @throws \LogicException if the "person" alias is not present and attaching accompanying period is not possible
     */
    protected function addJoinAccompanyingPeriod(QueryBuilder $query): void
    {
        if (FALSE === $this->havingAccompanyingPeriodInJoin($query)) {
            if (FALSE === \in_array('person', $query->getAllAliases())) {
                throw new \LogicException("the alias 'person' does not exists in "
                    . "query builder");
            }
            
            $query->join('person.accompanyingPeriods', 'accompanying_period');
        }
    }
}
