<?php
/*
 * Copyright (C) 2019 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Export\Filter;

use Chill\MainBundle\Export\FilterInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\QueryBuilder;
use Chill\MainBundle\Form\Type\ChillDateType;
use Doctrine\DBAL\Types\Type;
use Chill\PersonBundle\Export\AbstractAccompanyingPeriodExportElement;

/**
 * 
 *
 */
class AccompanyingPeriodOpeningFilter extends AbstractAccompanyingPeriodExportElement implements FilterInterface
{
    public function addRole()
    {
        return null;
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $this->addJoinAccompanyingPeriod($qb);
        
        $clause = $qb->expr()->andX(
            $qb->expr()->lte('accompanying_period.openingDate', ':date_to'),
            $qb->expr()->gte('accompanying_period.openingDate', ':date_from'));

        $qb->andWhere($clause);
        $qb->setParameter('date_from', $data['date_from'], Type::DATE);
        $qb->setParameter('date_to', $data['date_to'], Type::DATE);
    }

    public function applyOn(): string
    {
        return 'person';
    }

    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('date_from', ChillDateType::class, array(
            'label' => "Having an accompanying period opened after this date",
            'data'  => new \DateTime("-1 month"),
        ));
        
        $builder->add('date_to', ChillDateType::class, array(
            'label' => "Having an accompanying period opened before this date",
            'data'  => new \DateTime(),
        ));
    }

    public function describeAction($data, $format = 'string')
    {
        return [
            "Filtered by accompanying period: persons having an accompanying period"
            . " opened between the %date_from% and %date_to%",
            [
                '%date_from%' => $data['date_from']->format('d-m-Y'),
                '%date_to%'   => $data['date_to']->format('d-m-Y')
            ]
        ];
    }

    public function getTitle(): string
    {
        return "Filter by accompanying period: starting between two dates";
    }
}
