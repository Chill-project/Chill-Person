<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Export\Filter;

use Chill\MainBundle\Export\FilterInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Security\Core\Role\Role;
use Chill\MainBundle\Export\ExportElementValidatedInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Translation\TranslatorInterface;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class GenderFilter implements FilterInterface,
    ExportElementValidatedInterface
{
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

        public function applyOn()
    {
        return 'person';
    }

    /**
     *
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->add('accepted_genders', ChoiceType::class, array(
            'choices' => array(
                'Woman' => Person::FEMALE_GENDER,
                'Man' => Person::MALE_GENDER,
                'Both' => Person::BOTH_GENDER,
                'Not given' => 'null'
            ),
            'choices_as_values' => true,
            'multiple' => true,
            'expanded' => true
        ));
    }

    public function validateForm($data, ExecutionContextInterface $context)
    {
        if (!is_array($data['accepted_genders']) || count($data['accepted_genders']) === 0 ) {
            $context->buildViolation("You should select an option")
                ->addViolation();
        }
    }

    public function alterQuery(QueryBuilder $qb, $data)
    {
        $where = $qb->getDQLPart('where');
        $isIn = $qb->expr()->in('person.gender', ':person_gender');
        
        if (!\in_array('null', $data['accepted_genders'])) {
            $clause = $isIn;
        } else {
            $clause = $qb->expr()->orX($isIn, $qb->expr()->isNull('person.gender'));
        }

        if ($where instanceof Expr\Andx) {
            $where->add($clause);
        } else {
            $where = $qb->expr()->andX($clause);
        }

        $qb->add('where', $where);
        $qb->setParameter('person_gender', \array_filter(
            $data['accepted_genders'],
            function($el) {
                return $el !== 'null';
            }));
    }

    /**
     * A title which will be used in the label for the form
     *
     * @return string
     */
    public function getTitle()
    {
        return 'Filter by person gender';
    }

    public function addRole()
    {
        return NULL;
    }

    public function describeAction($data, $format = 'string')
    {
        $genders = [];
        
        foreach ($data['accepted_genders'] as $g) {
            if ('null' === $g) {
                $genders[] = $this->translator->trans('Not given');
            } else {
                $genders[] = $this->translator->trans($g);
            }
        }
        
        return [
            "Filtering by genders: only %genders%",
            [ "%genders%" => \implode(", ", $genders)]
        ];
    }
}
