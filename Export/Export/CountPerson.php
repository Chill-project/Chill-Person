<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\Query;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Export\Declarations;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CountPerson implements ExportInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    public function __construct(
            EntityManagerInterface $em
            )
    {
        $this->entityManager = $em;
    }
    
    /**
     * 
     */
    public function getType()
    {
        return Declarations::PERSON_TYPE;
    }
    
    public function getDescription()
    {
        return "Count peoples by various parameters.";
    }
    
    public function getTitle()
    {
        return "Count peoples";
    }
    
    public function requiredRole()
    {
        return new Role(PersonVoter::STATS);
    }
    
    /**
     * Initiate the query
     * 
     * @param QueryBuilder $qb
     * @return QueryBuilder
     */
    public function initiateQuery(array $requiredModifiers,  array $acl, array $data = array())
    {
        $centers = array_map(function($el) { return $el['center']; }, $acl);
        
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('COUNT(person.id) AS export_result')
                ->from('ChillPersonBundle:Person', 'person')
                ->join('person.center', 'center')
                ->andWhere('center IN (:authorized_centers)')
                ->setParameter('authorized_centers', $centers);
                ;
        
        
        return $qb;
    }
    
    public function getResult($qb, $data)
    {
        return $qb->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }
    
    public function getQueryKeys($data)
    {
        return array('export_result');
    }
    
    public function getLabels($key, array $values, $data)
    {
        if ($key !== 'export_result') {
            throw new \LogicException("the key $key is not used by this export");
        }
        
        $labels = array_combine($values, $values);
        $labels['_header'] = $this->getTitle();
        
        return function($value) use ($labels) {
            return $labels[$value];
        };
    }
    
    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_TABULAR);
    }
    
    public function buildForm(FormBuilderInterface $builder) {

    }
    
    public function supportsModifiers()
    {
        return array(Declarations::PERSON_TYPE, Declarations::PERSON_IMPLIED_IN);
    }
    
}
