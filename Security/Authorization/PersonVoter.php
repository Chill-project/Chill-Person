<?php

/*
 * Copyright (C) 2015 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Entity\User;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Entity\Center;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Role\Role;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PersonVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_PERSON_CREATE';
    const UPDATE = 'CHILL_PERSON_UPDATE';
    const SEE    = 'CHILL_PERSON_SEE';
    const STATS  = 'CHILL_PERSON_STATS';
    const LISTS  = 'CHILL_PERSON_LISTS';
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    protected function supports($attribute, $subject)
    {
        if ($subject instanceof Person) {
            return \in_array($attribute, [
                self::CREATE, self::UPDATE, self::SEE
            ]);
        } elseif ($subject instanceof Center) {
            return \in_array($attribute, [
                self::STATS, self::LISTS
            ]);
        } elseif ($subject === null) {
            return $attribute === self::CREATE;
        } else {
            return false;
        }
    }
    
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }
        
        if ($subject === null) {
            $centers = $this->helper->getReachableCenters($token->getUser(), 
                new Role($attribute));
            
            return count($centers) > 0;
        }
        
        return $this->helper->userHasAccess($token->getUser(), $subject, $attribute);
    }
    
    private function getAttributes()
    {
        return array(self::CREATE, self::UPDATE, self::SEE, self::STATS, self::LISTS);
    }

    public function getRoles()
    {
        return $this->getAttributes();
    }

    public function getRolesWithoutScope()
    {
        return $this->getAttributes();
    }
    
    public function getRolesWithHierarchy()
    {
        return [ 'Person' => $this->getRoles() ];
    }

}
