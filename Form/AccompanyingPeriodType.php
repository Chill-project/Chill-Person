<?php

namespace Chill\PersonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\MainBundle\Form\Type\UserPickerType;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Form\Type\ClosingMotivePickerType;

class AccompanyingPeriodType extends AbstractType
{
    /**
     * array of configuration for accompanying_periods.
     *
     * Contains whether we should add fields some optional fields (optional per
     * instance)
     *
     * @var string[]
     */
    protected $config = array();

    /**
     *
     * @param string[] $config configuration of visibility of some fields
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //if the period_action is close, date opening should not be seen
        if ($options['period_action'] !== 'close') {
            $builder
                ->add('openingDate', DateType::class, array(
                   "required" => true,
                   'widget' => 'single_text',
                   'format' => 'dd-MM-yyyy'
                   ));
        }

        // the closingDate should be seen only if period_action = close
        // or period_action = update AND accopanying period is already closed
        $builder->addEventListener(
              FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $accompanyingPeriod = $event->getData();
            $form = $event->getForm();

            //add date opening
            if (
                  //if the period_action is "close, should not be shown"
                  ($options['period_action'] === 'close')
                  OR
                  ($options['period_action'] === 'create')
                  OR
                  ($options['period_action'] === 'update' AND !$accompanyingPeriod->isOpen())
                  ) {
                $form->add('closingDate', DateType::class, array('required' => true,
                'widget' => 'single_text', 'format' => 'dd-MM-yyyy'));
                $form->add('closingMotive', ClosingMotivePickerType::class);
            }
        });
        
        if ($this->config['user'] === 'visible') {
            $builder->add('user', UserPickerType::class, [
                'center' => $options['center'],
                'role'   => new Role(PersonVoter::SEE),
            ]);
        }

        $builder->add('remark', TextareaType::class, array(
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\PersonBundle\Entity\AccompanyingPeriod'
        ));

        $resolver
            ->setRequired(array('period_action'))
            ->addAllowedTypes('period_action', 'string')
            ->addAllowedValues('period_action', array(
            'update', 'open', 'close', 'create'))
            ->setRequired('center')
            ->setAllowedTypes('center', \Chill\MainBundle\Entity\Center::class)
            ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['action'] = $options['period_action'];
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_personbundle_accompanyingperiod';
    }
}
