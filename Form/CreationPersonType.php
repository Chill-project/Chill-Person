<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Chill\MainBundle\Form\Type\CenterType;
use Chill\PersonBundle\Form\Type\GenderType;
use Chill\MainBundle\Form\Type\DataTransformer\CenterTransformer;
use Chill\PersonBundle\Config\ConfigPersonAltNamesHelper;
use Chill\PersonBundle\Form\Type\PersonAltNameType;

class CreationPersonType extends AbstractType
{

    const NAME = 'chill_personbundle_person_creation';

    const FORM_NOT_REVIEWED = 'not_reviewed';
    const FORM_REVIEWED = 'reviewed' ;
    const FORM_BEING_REVIEWED = 'being_reviewed';

    /**
     *
     * @var CenterTransformer
     */
    private $centerTransformer;
    
    /**
     *
     * @var ConfigPersonAltNamesHelper
     */
    protected $configPersonAltNamesHelper;

    public function __construct(
        CenterTransformer $centerTransformer,
        ConfigPersonAltNamesHelper $configPersonAltNamesHelper
     ) {
        $this->centerTransformer = $centerTransformer;
        $this->configPersonAltNamesHelper = $configPersonAltNamesHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['form_status'] === self::FORM_BEING_REVIEWED) {

            $dateToStringTransformer = new DateTimeToStringTransformer(
                    null, null, 'd-m-Y', false);

            $builder->add('firstName', HiddenType::class)
                    ->add('lastName', HiddenType::class)
                    ->add('birthdate', HiddenType::class, array(
                        'property_path' => 'birthdate'
                    ))
                    ->add('gender', HiddenType::class)
                    ->add('creation_date', HiddenType::class, array(
                        'mapped' => false
                    ))
                    ->add('form_status', HiddenType::class, array(
                        'mapped' => false,
                        'data' => $options['form_status']
                    ))
                    ->add('center', HiddenType::class)
                    ;
            
            if ($this->configPersonAltNamesHelper->hasAltNames()) {
                    $builder->add('altNames', PersonAltNameType::class, [
                        'by_reference' => false,
                        'force_hidden' => true
                ]);
            }
            
            $builder->get('birthdate')
                    ->addModelTransformer($dateToStringTransformer);
            $builder->get('creation_date')
                    ->addModelTransformer($dateToStringTransformer);
            $builder->get('center')
                    ->addModelTransformer($this->centerTransformer);
        } else {
            $builder
                ->add('firstName')
                ->add('lastName')
                ->add('birthdate', DateType::class, array('required' => false,
                    'widget' => 'single_text', 'format' => 'dd-MM-yyyy'))
                ->add('gender', GenderType::class, array(
                    'required' => true, 'placeholder' => null
                ))
                ->add('creation_date', DateType::class, array(
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'mapped' => false,
                    'data' => new \DateTime()))
                ->add('form_status', HiddenType::class, array(
                    'data' => $options['form_status'],
                    'mapped' => false
                    ))
                ->add('center', CenterType::class)
            ;
            
            if ($this->configPersonAltNamesHelper->hasAltNames()) {
                    $builder->add('altNames', PersonAltNameType::class, [
                        'by_reference' => false
                ]);
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\PersonBundle\Entity\Person'
        ));

        $resolver->setRequired('form_status')
              ->setAllowedValues('form_status', array(
                 self::FORM_BEING_REVIEWED,
                 self::FORM_NOT_REVIEWED,
                 self::FORM_REVIEWED
              ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return self::NAME;
    }
}
