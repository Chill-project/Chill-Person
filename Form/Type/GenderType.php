<?php

namespace Chill\PersonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Chill\PersonBundle\Entity\Person;

/**
 * A type to select the civil union state
 *
 * @author julien
 */
class GenderType extends AbstractType {

    public function getParent() {
        return ChoiceType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {

        $a = array(
            Person::MALE_GENDER => Person::MALE_GENDER,
            Person::FEMALE_GENDER => Person::FEMALE_GENDER,
            Person::BOTH_GENDER => Person::BOTH_GENDER
        );

        $resolver->setDefaults(array(
            'choices' => $a,
            'choices_as_values' => true,
            'expanded' => true,
            'multiple' => false,
            'placeholder' => null
        ));
    }

}
