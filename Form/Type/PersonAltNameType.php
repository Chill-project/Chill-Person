<?php

namespace Chill\PersonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\PersonBundle\Config\ConfigPersonAltNamesHelper;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * 
 *
 */
class PersonAltNameType extends AbstractType
{
    /**
     *
     * @var ConfigPersonAltNamesHelper
     */
    private $configHelper;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;
    
    public function __construct(
        ConfigPersonAltNamesHelper $configHelper, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configHelper = $configHelper;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->getKeyChoices() as $label => $key) {
            $builder->add(
                $key, 
                $options['force_hidden'] ? HiddenType::class : TextType::class, [
                'label' => $label,
                'required' => false
            ]);
        }
        
        $builder->setDataMapper(new \Chill\PersonBundle\Form\DataMapper\PersonAltNameDataMapper());
    }
    
    protected function getKeyChoices()
    {
        $choices = $this->configHelper->getChoices();
        $translatedChoices = [];
        
        foreach ($choices as $key => $labels) {
            $label = $this->translatableStringHelper->localize($labels);
            $translatedChoices[$label] = $key;
        }
        
        return $translatedChoices;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', \Chill\PersonBundle\Entity\PersonAltName::class)
            ->setDefined('force_hidden')
            ->setAllowedTypes('force_hidden', 'bool')
            ->setDefault('force_hidden', false)
            ;
    }

}
