<?php

namespace Chill\PersonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Chill\MainBundle\Templating\Entity\ChillEntityRenderExtension;
use Symfony\Component\OptionsResolver\Options;
use Chill\PersonBundle\Repository\ClosingMotiveRepository;

/**
 * A type to add a closing motive
 *
 */
class ClosingMotivePickerType extends AbstractType
{

    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var ChillEntityRenderExtension
     */
    protected $entityRenderExtension;
    
    /**
     *
     * @var ClosingMotiveRepository
     */
    protected $repository;
    
    public function __construct(
        TranslatableStringHelper $translatableStringHelper,
        ChillEntityRenderExtension $chillEntityRenderExtension,
        ClosingMotiveRepository $closingMotiveRepository
    ) {
        $this->translatableStringHelper = $translatableStringHelper;
        $this->entityRenderExtension = $chillEntityRenderExtension;
        $this->repository = $closingMotiveRepository;
    }

        public function getBlockPrefix()
    {
        return 'closing_motive';
    }

    public function getParent()
    {
        return EntityType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'class' => ClosingMotive::class,
                'empty_data' => null,
                'placeholder' => 'Choose a motive',
                'choice_label' => function(ClosingMotive $cm) {
                    return $this->entityRenderExtension->renderString($cm);
                },
                'only_leaf' => true
                )
            );
                
        $resolver
            ->setAllowedTypes('only_leaf', 'bool')
            ->setNormalizer('choices', function (Options $options) {
                return $this->repository->getActiveClosingMotive($options['only_leaf']);
            });
    }

}
