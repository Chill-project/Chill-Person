<?php
/*
 * 
 * Copyright (C) 2014-2020, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\PersonBundle\Form\Type\ClosingMotivePickerType;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * 
 *
 */
class ClosingMotiveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TranslatableStringFormType::class, [
                'label' => 'Nom'
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Actif ?',
                'required' => false
            ])
            ->add('ordering', NumberType::class, [
                'label' => 'Ordre d\'apparition',
                'required' => true,
                'scale' => 5
            ])
            ->add('parent', ClosingMotivePickerType::class, [
                'label' => 'Parent',
                'required' => false,
                'placeholder' => 'closing_motive.any parent',
                'multiple' => false,
                'only_leaf' => false
            ])
            ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('class', \Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive::class)
            ;
    }
}
 