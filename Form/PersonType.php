<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Chill\PersonBundle\Form\Type\GenderType;
use Chill\MainBundle\Form\Type\Select2CountryType;
use Chill\MainBundle\Form\Type\Select2LanguageType;
use Chill\CustomFieldsBundle\Form\Type\CustomFieldType;
use Chill\PersonBundle\Form\Type\Select2MaritalStatusType;
use Chill\PersonBundle\Config\ConfigPersonAltNamesHelper;
use Chill\PersonBundle\Form\Type\PersonAltNameType;

class PersonType extends AbstractType
{
    /**
     * array of configuration for person_fields.
     *
     * Contains whether we should add fields some optional fields (optional per
     * instance)
     *
     * @var string[]
     */
    protected $config = array();
    
    /**
     *
     * @var ConfigPersonAltNamesHelper
     */
    protected $configAltNamesHelper;

    /**
     *
     * @param string[] $personFieldsConfiguration configuration of visibility of some fields
     */
    public function __construct(
        array $personFieldsConfiguration,
        ConfigPersonAltNamesHelper $configAltNamesHelper
    ) {
        $this->config = $personFieldsConfiguration;
        $this->configAltNamesHelper = $configAltNamesHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('birthdate', DateType::class, array('required' => false, 'widget' => 'single_text', 'format' => 'dd-MM-yyyy'))
            ->add('gender', GenderType::class, array(
                'required' => true
            ));
        
        if ($this->configAltNamesHelper->hasAltNames()) {
            $builder->add('altNames', PersonAltNameType::class, [
                'by_reference' => false
            ]);
        }
        
        if ($this->config['memo'] === 'visible') {
            $builder
                ->add('memo', TextareaType::class, array('required' => false))
            ;
        }

        if ($this->config['place_of_birth'] === 'visible') {
            $builder->add('placeOfBirth', TextType::class, array('required' => false));
        }

        if ($this->config['contact_info'] === 'visible') {
            $builder->add('contactInfo', TextareaType::class, array('required' => false));
        }

        if ($this->config['phonenumber'] === 'visible') {
            $builder->add('phonenumber', TelType::class, array('required' => false));
        }
        
        if ($this->config['mobilenumber'] === 'visible') {
            $builder->add('mobilenumber', TelType::class, array('required' => false));
        }

        if ($this->config['email'] === 'visible') {
            $builder->add('email', EmailType::class, array('required' => false));
        }

        if ($this->config['country_of_birth'] === 'visible') {
            $builder->add('countryOfBirth', Select2CountryType::class, array(
                'required' => false
                ));
        }

        if ($this->config['nationality'] === 'visible') {
            $builder->add('nationality', Select2CountryType::class, array(
                'required' => false
                ));
        }

        if ($this->config['spoken_languages'] === 'visible') {
            $builder->add('spokenLanguages', Select2LanguageType::class, array(
                'required' => false,
                'multiple' => true
                ));
        }

        if ($this->config['marital_status'] === 'visible'){
            $builder->add('maritalStatus', Select2MaritalStatusType::class, array(
                'required' => false
                ));
        }

        if($options['cFGroup']) {
            $builder
                ->add('cFData', CustomFieldType::class,
                array('attr' => array('class' => 'cf-fields'), 'group' => $options['cFGroup']))
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\PersonBundle\Entity\Person',
            'validation_groups' => array('general', 'creation')
        ));

        $resolver->setRequired(array(
            'cFGroup'
        ));

        $resolver->setAllowedTypes(
            'cFGroup', array('null', 'Chill\CustomFieldsBundle\Entity\CustomFieldsGroup')
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_personbundle_person';
    }
}
