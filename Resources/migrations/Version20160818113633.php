<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * This migration store the cfdata using the postgresql jsonb type instead
 * of the result of the sterialization
 */
class Version20160818113633 extends AbstractMigration
{
    /**
     * Make a copy of the column cfdata into the column cfdata_old. Then
     * remplace the sterialized data into a json data.
     *
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $personIdAndCFData = $this->connection->executeQuery('SELECT id, cfdata FROM person');

        $this->addSQL('ALTER TABLE person RENAME COLUMN cfdata TO cfdata_old');
        $this->addSql('ALTER TABLE person ALTER COLUMN cfdata_old DROP NOT NULL');
        $this->addSQL('ALTER TABLE person ADD COLUMN cfdata jsonb');

        foreach ($personIdAndCFData as $person) {
            $personId = $person['id'];
            $cFDataArray = unserialize($person['cfdata']);
            $cFDataJson = json_encode($cFDataArray);
            $this->addSql(
                'UPDATE person set cfdata = :cfdatajson WHERE id = :id',
                ['cfdatajson' => $cFDataJson, 'id' => $personId]
            );
        }
    }

    /**
     * Inverse of up
     *
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSQL('ALTER TABLE person DROP COLUMN cfdata');
        $this->addSQL('ALTER TABLE person RENAME COLUMN cfdata_old TO cfdata');
        $this->addSql('ALTER TABLE person ALTER COLUMN cfdata SET NOT NULL');
    }
}
