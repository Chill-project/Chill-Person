<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Correction: copie les infos de email vers contactInfo.
 * 
 * Previously, data from contact where stored in 'email' column
 */
final class Version20180820120000 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE chill_person_person SET contactInfo=email');
        $this->addSql('UPDATE chill_person_person SET email=\'\'');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE chill_person_person SET email=contactInfo');
        $this->addSql('UPDATE chill_person_person SET contactInfo=\'\'');
    }
}
