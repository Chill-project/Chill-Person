<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add a contactInfo and a mobilenumber columns on person. Change the email column.
 */
final class Version20180518144221 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_person_person ADD contactInfo TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ADD mobilenumber TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE chill_person_person ALTER email DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_person_person DROP contactInfo');
        $this->addSql('ALTER TABLE chill_person_person DROP mobilenumber');
        $this->addSql('ALTER TABLE chill_person_person ALTER email SET NOT NULL');
    }
}
