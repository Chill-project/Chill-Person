<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\DependencyInjection\Widget\Factory\AbstractWidgetFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

/**
 * add configuration for the person_list widget.
 */
class PersonListWidgetFactory extends AbstractWidgetFactory
{
    public function configureOptions($place, NodeBuilder $node)
    {
        $node->booleanNode('only_active')
                ->defaultTrue()
                ->end();
        $node->integerNode('number_of_items')
            ->defaultValue(50)
            ->end();
        $node->scalarNode('filtering_class')
                ->defaultNull()
                ->end();
        $node->arrayNode('custom_fields')
                ->prototype('scalar')->end()
                ->info("Add some custom field to the view. Add the slug of some custom field"
                        . " if you want to override the view and show their value in the list")
                ->example(array("custom-field-slug-1", "custom-field-slug-2"))
                ->cannotBeEmpty()
                ->end();
    }
        
    public function getAllowedPlaces()
    {
        return array('homepage');
    }
    
    public function getWidgetAlias()
    {
        return 'person_list';
    }
    
    public function getServiceId(ContainerBuilder $containerBuilder, $place, $order, array $config)
    {
        return 'chill_person.widget.person_list';
    }
    
}
