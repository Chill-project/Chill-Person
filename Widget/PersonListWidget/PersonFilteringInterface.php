<?php


/*
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Widget\PersonListWidget;

use Doctrine\ORM\EntityManager;
use Chill\MainBundle\Entity\User;


/**
 * Interface to implement on classes called in configuration for
 * PersonListWidget (`person_list`), under the key `filtering_class` :
 * 
 * ```
 * widgets:
 *      homepage:
 *          person_list:
 *              # where \FQDN\To\Class implements PersonFiltering
 *              class_filtering: \FQDN\To\Class
 * ```
 * 
 */
interface PersonFilteringInterface 
{
    /**
     * Return an array of persons id to show.
     * 
     * Those ids are inserted into the query like this (where ids is the array 
     * returned by this class) :
     * 
     * ```
     * SELECT p FROM ChillPersonBundle:Persons p 
     * WHERE p.id IN (:ids)
     * AND 
     * -- security/authorization statement: restraint person to authorized centers
     * p.center in :authorized_centers
     * ```
     * 
     * Example of use : filtering based on custom field data :
     * ```
 
    class HomepagePersonFiltering implements PersonFilteringInterface {

    public function getPersonIds(EntityManager $em, User $user)
    {
        $rsmBuilder = new ResultSetMappingBuilder($em);
        $rsmBuilder->addScalarResult('id', 'id', Type::BIGINT);
        
        $personTable = $em->getClassMetadata('ChillPersonBundle:Person')
                ->getTableName();
        $personIdColumn = $em->getClassMetadata('ChillPersonBundle:Person')
                ->getColumnName('id');
        $personCfDataColumn = $em->getClassMetadata('ChillPersonBundle:Person')
                ->getColumnName('cfData');
        
        return $em->createNativeQuery(sprintf("SELECT %s FROM %s WHERE "
                . "jsonb_exists(%s, 'school-2fb5440e-192c-11e6-b2fd-74d02b0c9b55')",
                $personIdColumn, $personTable, $personCfDataColumn), $rsmBuilder)
                ->getScalarResult();
    }
}
     * ```
     * 
     * @param EntityManager $em
     * @return int[] an array of persons id to show
     */
    public function getPersonIds(EntityManager $em, User $user);
}
