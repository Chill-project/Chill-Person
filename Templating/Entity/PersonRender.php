<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\AbstractChillEntityRender;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Config\ConfigPersonAltNamesHelper;

/**
 * Render a Person
 *
 */
class PersonRender extends AbstractChillEntityRender
{
    /**
     *
     * @var ConfigPersonAltNamesHelper
     */
    protected $configAltNamesHelper;
    
    public function __construct(ConfigPersonAltNamesHelper $configAltNamesHelper)
    {
        $this->configAltNamesHelper = $configAltNamesHelper;
    }
    
    /**
     * 
     * @param Person $person
     * @param array $options
     * @return string
     */
    public function renderBox($person, array $options): string
    {
        return 
            $this->getDefaultOpeningBox('person').
            '<span class="chill-entity__person__first-name">'.$person->getFirstName().'</span>'.
            ' <span class="chill-entity__person__last-name">'.$person->getLastName().'</span>'.
            $this->addAltNames($person, true).
            $this->getDefaultClosingBox()
            ;
    }

    /**
     * 
     * @param Person $person
     * @param array $options
     * @return string
     */
    public function renderString($person, array $options): string
    {
        return $person->getFirstName().' '.$person->getLastName()
            .$this->addAltNames($person, false);
    }
    
    protected function addAltNames(Person $person, bool $addSpan)
    {
        $str = '';
        
        if ($this->configAltNamesHelper->hasAltNames()) {
            $altNames = $this->configAltNamesHelper->getChoices();
            $isFirst = true;

            foreach ($person->getAltNames()->getIterator() as $altName) {
                /** @var \Chill\PersonBundle\Entity\PersonAltName $altName */
                if (\array_key_exists($altName->getKey(), $altNames)) {
                    if ($isFirst) {
                        $str .= " (";
                        $isFirst = false;
                    } else {
                        $str.= " ";
                    }
                    if ($addSpan) {
                        $str .= '<span class="chill-entity__person__alt-name chill-entity__person__altname--'.$altName->getKey().'">';
                    }
                    $str .= $altName->getLabel();
                    
                    if ($addSpan) {
                        $str .= "</span>";
                    }
                }
            }
            
            if (!$isFirst) {
                $str .= ")";
            }
        }
        
        return $str;
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof Person;
    }
}
