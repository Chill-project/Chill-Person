<?php

namespace Chill\PersonBundle\Templating\Entity;

use Chill\MainBundle\Templating\Entity\AbstractChillEntityRender;
use Chill\PersonBundle\Entity\AccompanyingPeriod\ClosingMotive;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * Render closing motive
 *
 */
class ClosingMotiveRender extends AbstractChillEntityRender
{
    private CONST SEPARATOR = ' > ';
    
    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    public function renderBox($entity, array $options): string
    {
        return 
            $this->getDefaultOpeningBox('closing-motive').
            $this->renderString($entity, $options).
            $this->getDefaultClosingBox()
            ;
    }

    /**
     * 
     * @param ClosingMotive $entity
     * @param array $options
     * @return string
     */
    public function renderString($entity, array $options): string
    {
        return $this->renderStringRecursive($entity, 
            '', //$this->translatableStringHelper->localize($entity->getName()), 
            $options);
    }
    
    protected function renderStringRecursive(ClosingMotive $motive, $existing, array $options)
    {
        $newExisting = $this->translatableStringHelper->localize($motive->getName());
        
        if ($motive->hasParent()) {
            
            if (!empty($existing)) {
                $newExisting = $newExisting.self::SEPARATOR.$existing;
            }
            
            return $this->renderStringRecursive($motive->getParent(), $newExisting,
                $options);
        } else {
            if (!empty($existing)) {
                return $newExisting.self::SEPARATOR.$existing;
            } else {
                return $newExisting;
            }
        }
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof ClosingMotive;
    }
}
