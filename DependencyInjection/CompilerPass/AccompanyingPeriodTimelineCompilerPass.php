<?php
/*
 * Copyright (C) 2018 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Remove services which add AccompanyingPeriod to timeline if 
 * accompanying_periods are set to `hidden` 
 *
 */
class AccompanyingPeriodTimelineCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // remove services when accompanying period are hidden
        if ($container->getParameter('chill_person.accompanying_period') !== 'hidden') {
            return;
        }
        
        $definitions = [
            'chill.person.timeline.accompanying_period_opening',
            'chill.person.timeline.accompanying_period_closing'
        ];
        
        foreach($definitions as $definition) {
            $container
                ->removeDefinition($definition)
                ;
        }
        
        $definition = $container->getDefinition('chill.main.timeline_builder');
        
        // we have to remove all methods call, and re-add them if not linked 
        // to this service
        $calls = $definition->getMethodCalls();
        
        foreach($calls as list($method, $arguments)) {
            if ($method !== 'addProvider') {
                continue;
            }
            
            $definition->removeMethodCall('addProvider');
            
            if (FALSE === \in_array($arguments[1], $definitions)) {
                $definition->addMethodCall($method, $arguments);
            } 
        }
    }
}
