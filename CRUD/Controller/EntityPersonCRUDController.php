<?php
/*
 * Copyright (C) 2016-2019 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\CRUD\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Chill\PersonBundle\Entity\Person;
use Doctrine\ORM\QueryBuilder;

/**
 * CRUD Controller for entities attached to a Person
 */
class EntityPersonCRUDController extends CRUDController
{
    /**
     * Extract the person from the request
     * 
     * the person parameter will be `person_id` and must be 
     * present in the query
     * 
     * If the parameter is not set, this method will return null.
     * 
     * If the person id does not exists, the method will throw a 
     * Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * 
     * @param Request $request
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException if the person with given id is not found
     * @return Person|null
     */
    protected function getPerson(Request $request): ?Person
    {
        if (FALSE === $request->query->has('person_id')) {
            return null;
        }
        
        $person = $this->getDoctrine()
            ->getRepository(Person::class)
            ->find($request->query->getInt('person_id'))
            ;
        
        if (NULL === $person) {
            throw $this->createNotFoundException('the person with this id is not found');
        }
        
        return $person;
    }
    
    protected function createEntity($action, Request $request): object
    {       
        $entity = parent::createEntity($action, $request);
        
        $person = $this->getPerson($request);
        
        $entity->setPerson($person);
        
        return $entity;
    }
    
    protected function generateTemplateParameter(string $action, $entity, Request $request, array $defaultTemplateParameters = array()): array
    {
        $person = $this->getPerson($request);
        
        if (NULL === $person) {
            throw new \Exception("the `person_id` parameter is not set in the query. "
                . "You should set it or override the current method to allow another "
                . "behaviour: ".__METHOD__);
        }
        
        return parent::generateTemplateParameter(
            $action, 
            $entity, 
            $request, 
            \array_merge([ 'person' => $person ], $defaultTemplateParameters)
            );
    }
    
    protected function getTemplateFor($action, $entity, Request $request)
    {
        if ($this->hasCustomTemplate($action, $entity, $request)) {
            return $this->getActionConfig($action)['template'];
        }
        
        switch ($action) {
            case 'new':
                return '@ChillPerson/CRUD/new.html.twig';
            case 'edit': 
                return '@ChillPerson/CRUD/edit.html.twig';
            case 'view':
                return '@ChillPerson/CRUD/view.html.twig';
            case 'delete':
                return '@ChillPerson/CRUD/delete.html.twig';
            case 'index':
                return '@ChillPerson/CRUD/index.html.twig';
            default:
                return parent::getTemplateFor($action, $entity, $request);
        }
    }
    
    protected function onBeforeRedirectAfterSubmission(string $action, $entity, \Symfony\Component\Form\FormInterface $form, Request $request)
    {
        $next = $request->request->get("submit", "save-and-close");
            
        switch ($next) {
            case "save-and-close":
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_index', [
                    'person_id' => $this->getPerson($request)->getId()
                ]);
            case "save-and-new":
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_new', [
                    'person_id' => $this->getPerson($request)->getId()
                ]);
            case "new":
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_view', [
                    'id' => $entity->getId(),
                    'person_id' => $this->getPerson($request)->getId()
                ]);
            default:
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_view', [
                    'id' => $entity->getId(),
                    'person_id' => $this->getPerson($request)->getId()
                    ]);
        }
    }
    
    /**
     * Override the base method to add a filtering step to a person.
     * 
     * @param string $action
     * @param Request $request
     * @return QueryBuilder
     */
    protected function buildQueryEntities(string $action, Request $request)
    {
        $qb = parent::buildQueryEntities($action, $request);
        
        return $this->filterQueryEntitiesByPerson($action, $qb, $request);
    }
    
    /**
     * Add a where clause to the buildQuery
     * 
     * @param string $action
     * @param \Chill\PersonBundle\CRUD\Controller\QueryBuilder $qb
     * @param Request $request
     * @return \Chill\PersonBundle\CRUD\Controller\QueryBuilder
     */
    protected function filterQueryEntitiesByPerson(string $action, QueryBuilder $qb, Request $request): QueryBuilder
    {
        $qb->andWhere($qb->expr()->eq('e.person', ':person'));
        $qb->setParameter('person', $this->getPerson($request));
        
        return $qb;
    }
}
