<?php
/*
 * Copyright (C) 2016-2019 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\CRUD\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for entities attached as one-to-on to a person
 * 
 */
class OneToOneEntityPersonCRUDController extends CRUDController
{
    protected function getTemplateFor($action, $entity, Request $request)
    {
        if (!empty($this->crudConfig[$action]['template'])) {
            return $this->crudConfig[$action]['template'];
        }
        
        switch ($action) {
            case 'new':
                return '@ChillPerson/CRUD/new.html.twig';
            case 'edit': 
                return '@ChillPerson/CRUD/edit.html.twig';
            case 'index':
                return '@ChillPerson/CRUD/index.html.twig';
            default:
                throw new \LogicException("the view for action $action is not "
                    . "defined. You should override ".__METHOD__." to add this "
                    . "action");
        }
    }
    
    protected function getEntity($action, $id, Request $request): ?object
    {
        $entity = parent::getEntity($action, $id, $request);
        
        if (NULL === $entity) {
            $entity = $this->createEntity($action, $request);
            $person = $this->getDoctrine()
                ->getManager()
                ->getRepository(Person::class)
                ->find($id);
            
            $entity->setPerson($person);
        }
        
        return $entity;
    }
    
    protected function onPreFlush(string $action, $entity, FormInterface $form, Request $request)
    {
        $this->getDoctrine()->getManager()->persist($entity);
    }
    
    protected function onPostFetchEntity($action, Request $request, $entity): ?Response
    {
        if (FALSE === $this->getDoctrine()->getManager()->contains($entity)) {
            return new RedirectResponse($this->generateRedirectOnCreateRoute($action, $request, $entity));
        }
        
        return null;
    }
    
    protected function generateRedirectOnCreateRoute($action, Request $request, $entity)
    {
        throw new BadMethodCallException("not implemtented yet");
    }

}
