<?php

/*
 * Copyright (C) 2015 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\PersonBundle\Controller;

use Chill\PersonBundle\Privacy\PrivacyEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TimelinePersonController extends Controller
{
    
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    
    /**
     * TimelinePersonController constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }
    
    
    public function personAction(Request $request, $person_id)
    {
        $person = $this->getDoctrine()
                ->getRepository('ChillPersonBundle:Person')
                ->find($person_id);

        if ($person === NULL) {
            throw $this->createNotFoundException();
        }
        
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $person);
        
        /* @var $timelineBuilder \Chill\MainBundle\Timeline\TimelineBuilder */
        $timelineBuilder = $this->get('chill.main.timeline_builder');
        $paginatorFactory = $this->get('chill_main.paginator_factory');
        
        $nbItems = $timelineBuilder->countItems('person', 
            [ 'person' => $person ]
            );
        
        $paginator = $paginatorFactory->create($nbItems);
        
        $event = new PrivacyEvent($person, array('action' => 'timeline'));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        
        return $this->render('ChillPersonBundle:Timeline:index.html.twig', array
            (
                'timeline' => $timelineBuilder->getTimelineHTML(
                    'person', 
                    array('person' => $person),
                    $paginator->getCurrentPage()->getFirstItemNumber(),
                    $paginator->getItemsPerPage()
                    ),
                'person' => $person,
                'nb_items' => $nbItems,
                'paginator' => $paginator
            )
        );
    }

}
