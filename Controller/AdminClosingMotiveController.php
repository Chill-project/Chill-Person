<?php

namespace Chill\PersonBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Chill\MainBundle\Pagination\PaginatorInterface;

/**
 * Controller for closing motives
 *
 */
class AdminClosingMotiveController extends CRUDController
{
    protected function createEntity($action, Request $request): object
    {
        $entity = parent::createEntity($action, $request);
        
        if ($request->query->has('parent_id')) {
            $parentId = $request->query->getInt('parent_id');
            
            $parent = $this->getDoctrine()->getManager()
                ->getRepository($this->getEntityClass())
                ->find($parentId);
            
            if (NULL === $parent) {
                throw $this->createNotFoundException('parent id not found');
            }
            
            $entity->setParent($parent);
        }
        
        return $entity;
    }
    
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        /** @var \Doctrine\ORM\QueryBuilder $query */
        return $query->orderBy('e.ordering', 'ASC');
    }
}
