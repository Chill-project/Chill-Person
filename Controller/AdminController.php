<?php

namespace Chill\PersonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * 
     */
    public function indexAction($_locale)
    {
        return $this->render('ChillPersonBundle:Admin:index.html.twig', array(
            // ...
        ));
    }

}
