<?php

namespace Chill\PersonBundle\Config;


/**
 * Give help to interact with the config for alt names
 * 
 *
 */
class ConfigPersonAltNamesHelper
{
    /**
     * the raw config, directly from the container parameter
     *
     * @var array
     */
    private $config = [];
    
    public function __construct($config)
    {
        $this->config = $config;
    }
    
    /**
     * Return true if at least one alt name is configured
     * 
     * @return bool
     */
    public function hasAltNames(): bool
    {
        return count($this->config) > 0;
    }
    
    /**
     * get the choices as key => values
     * 
     * @return array
     */
    public function getChoices(): array
    {
        $choices = [];
        foreach ($this->config as $entry) {
            
            $labels = $entry['labels'];
            $lang = false;
            $label = false;
            $cur = reset($labels);
            while ($cur) {
                if (key($labels) === 'lang') {
                    $lang = current($labels);
                }
                
                if (key($labels) === 'label') {
                    $label = current($labels);
                }
                
                if ($lang !== FALSE && $label !== FALSE) {
                    $choices[$entry['key']][$lang] = $label;
                    $lang = false;
                    $label = false;
                }
                $cur = next($labels);
            }
        }
        
        return $choices;
    }

}
