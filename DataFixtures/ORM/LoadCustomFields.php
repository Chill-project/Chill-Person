<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\CustomFieldsBundle\CustomFields\CustomFieldTitle;
use Chill\CustomFieldsBundle\CustomFields\CustomFieldText;
use Chill\CustomFieldsBundle\CustomFields\CustomFieldChoice;
use Chill\CustomFieldsBundle\Entity\CustomFieldsDefaultGroup;
use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadCustomFields extends AbstractFixture implements OrderedFixtureInterface,
  ContainerAwareInterface
{
    /**
     *
     * @var ContainerInterface
     */
    private $container;
    
    /**
     *
     * @var CustomField
     */
    private $customFieldText;
    
    /**
     *
     * @var CustomField
     */
    private $customFieldChoice;
    
    //put your code here
    public function getOrder()
    {
        return 10003;
    }
    
    public function setContainer(ContainerInterface $container = null)
    {
        if ($container === null) {
            throw new \RuntimeException("The given container should not be null");
        }
        
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadFields($manager);
        $this->loadData($manager);
        $manager->flush();
    }
    
    private function loadData(ObjectManager $manager)
    {
        $personIds = $this->container->get('doctrine.orm.entity_manager')
            ->createQuery("SELECT person.id FROM ChillPersonBundle:Person person")
            ->getScalarResult();
        
        // get possible values for cfGroup
        $choices = array_map(
            function($a) { return $a["slug"]; }, 
            $this->customFieldChoice->getOptions()["choices"]
        );
        // create faker
        $faker = \Faker\Factory::create('fr_FR');
        // select a set of people and add data
        foreach ($personIds as $id) {
            // add info on 1 person on 2
            if (rand(0,1) === 1) {
                /* @var $person Person */
                $person = $manager->getRepository(Person::class)->find($id);
                $person->setCFData(array(
                    "remarques" => $this->createCustomFieldText()
                        ->serialize($faker->text(rand(150, 250)), $this->customFieldText),
                    "document-d-identite" => $this->createCustomFieldChoice()
                        ->serialize(array($choices[array_rand($choices)]), $this->customFieldChoice)
                ));
            }
        }
    }
    
    private function createCustomFieldText()
    {
        return new CustomFieldText(
            $this->container->get('request_stack'),
            $this->container->get('templating'),
            $this->container->get('chill.main.helper.translatable_string')
        );
    }
    
    private function createCustomFieldChoice()
    {
        return new CustomFieldChoice(
            $this->container->get('translator.default'),
            $this->container->get('templating'),
            $this->container->get('chill.main.helper.translatable_string')
        );
    }
    
    private function loadFields(ObjectManager $manager)
    {
        $cfGroup = (new CustomFieldsGroup())
            ->setEntity(Person::class)
            ->setName(array("fr" => "Données"))
            ;
        $manager->persist($cfGroup);
        
        // make this group default for Person::class
        $manager->persist(
            (new CustomFieldsDefaultGroup())
                ->setCustomFieldsGroup($cfGroup)
                ->setEntity(Person::class)
            );
        
        // create title field
        $customField0 = (new CustomField())
            ->setActive(true)
            ->setName(array("fr" => "Données personnalisées"))
            ->setSlug("personal-data")
            ->setOrdering(10)
            ->setType('title')
            ->setOptions(array(CustomFieldTitle::TYPE => CustomFieldTitle::TYPE_TITLE))
            ->setCustomFieldsGroup($cfGroup)
            ;
        $manager->persist($customField0);
        
        // create text field
        $this->customFieldText = (new CustomField())
            ->setActive(true)
            ->setName(array("fr" => "Remarques"))
            ->setSlug("remarques")
            ->setOrdering(20)
            ->setType('text')
            ->setOptions(array('maxLength' => 5000))
            ->setCustomFieldsGroup($cfGroup)
            ;
        $manager->persist($this->customFieldText);
        
        // create choice field
        $this->customFieldChoice = (new CustomField())
            ->setActive(true)
            ->setName(array("fr" => "Document d'identité"))
            ->setSlug("document-d-identite")
            ->setOrdering(30)
            ->setType('choice')
            ->setCustomFieldsGroup($cfGroup)
            ->setOptions(array(
                "multiple" => true,
                "other"    => false,
                "expanded" => true,
                "active"   => true,
                "slug"     => "document-d-identite",
                "choices"  => array(
                    array(
                        "name"   => array("fr" => "Carte d'identité"),
                        "active" => true,
                        "slug"   => "carte-d-identite"
                    ),
                    array(
                        "name"   => array("fr" => "Passeport"),
                        "active" => true,
                        "slug"   => "passeport"
                    ),
                    array(
                        "name"   => array("fr" => "Titre de séjour"),
                        "active" => true,
                        "slug"   => "passeport"
                    )
                )
            ))
            ;
        $manager->persist($this->customFieldChoice);
    }

}
