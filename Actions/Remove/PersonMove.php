<?php
/*
 * Copyright (C) 2016-2019 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Actions\Remove;

use Doctrine\ORM\EntityManagerInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\PersonBundle\Entity\AccompanyingPeriod;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Chill\PersonBundle\Actions\ActionEvent;

/**
 * Move or delete entities associated to a person to a new one, and delete the 
 * old person. The data associated to a person (birthdate, name, ...) are left 
 * untouched on the "new one".
 * 
 * See `getSql` for details.
 * 
 *
 */
class PersonMove
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    
    public function __construct(
        EntityManagerInterface $em, 
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Return the sql used to move or delete entities associated to a person to 
     * a new one, and delete the old person. The data associated to a person 
     * (birthdate, name, ...) are left untouched on the "new one".
     * 
     * The accompanying periods associated to a person are always removed. The other 
     * associated entity are updated: the new person id is associated to the entity.
     * 
     * Optionnaly, you can ask for removing entity by passing them in $deleteEntities
     * parameters. 
     * 
     * The following events are triggered:
     * - `'CHILL_PERSON.DELETE_ASSOCIATED_ENTITY'` is triggered when an entity
     *     will be removed ;
     * - `'CHILL_PERSON.MOVE_ASSOCIATED_ENTITY'` is triggered when an entity
     *     will be moved ;
     * 
     * Those events have the following metadata:
     * 
     * - 'original_action' : always 'move' ;
     * - 'to': the person id to move ;
     * 
     * @param Person $from
     * @param Person $to
     * @param array $deleteEntities
     * @return type
     */
    public function getSQL(Person $from, Person $to, array $deleteEntities = [])
    {
        $sqls = [];
        $toDelete = \array_merge($deleteEntities, $this->getDeleteEntities());
        
        foreach ($this->em->getMetadataFactory()->getAllMetadata() as $metadata) {
            if ($metadata->isMappedSuperclass) {
                continue;
            }
            
            foreach ($metadata->getAssociationMappings() as $field => $mapping) {
                if ($mapping['targetEntity'] === Person::class) {
                    
                    if (\in_array($metadata->getName(), $toDelete)) {
                        $sql = $this->createDeleteSQL($metadata, $from, $field);
                        $event = new ActionEvent($from->getId(), $metadata->getName(), $sql, 
                            ['to' => $to->getId(), 'original_action' => 'move']);
                        $this->eventDispatcher->dispatch(ActionEvent::DELETE, $event);
                        
                    } else {
                        $sql = $this->createMoveSQL($metadata, $from, $to, $field);
                        $event = new ActionEvent($from->getId(), $metadata->getName(), $sql, 
                            ['to' => $to->getId(), 'original_action' => 'move']);
                        $this->eventDispatcher->dispatch(ActionEvent::MOVE, $event);
                    }

                    $sqls = \array_merge($sqls, $event->getPreSql(), [$event->getSqlStatement()], $event->getPostSql());
                }
            }
        }
        
        $personMetadata = $this->em->getClassMetadata(Person::class);
        $sqls[] = sprintf("DELETE FROM %s WHERE id = %d", 
            $this->getTableName($personMetadata),
            $from->getId());
        
        return $sqls ?? [];
    }
    
    protected function createMoveSQL(ClassMetadata $metadata, Person $from, Person $to, $field): string
    {
        $mapping = $metadata->getAssociationMapping($field);
        
        // Set part of the query, aka <here> in  "UPDATE table SET <here>                                                                                                                                                                            "
        $sets = [];
        foreach ($mapping["joinColumns"] as $columns) {
            $sets[] = sprintf("%s = %d", $columns["name"], $to->getId());
        }
        
        $conditions = [];
        foreach ($mapping["joinColumns"] as $columns) {
            $conditions[] = sprintf("%s = %d", $columns["name"], $from->getId());
        }
        
        return \sprintf("UPDATE %s SET %s WHERE %s",
            $this->getTableName($metadata),
            \implode(" ", $sets),
            \implode(" AND ", $conditions)
            );
    }
    
    protected function createDeleteSQL(ClassMetadata $metadata, Person $from, $field): string
    {
        $mapping = $metadata->getAssociationMapping($field);
        
        $conditions = [];
        foreach ($mapping["joinColumns"] as $columns) {
            $conditions[] = sprintf("%s = %d", $columns["name"], $from->getId());
        }
        
         return \sprintf("DELETE FROM %s WHERE %s",
            $this->getTableName($metadata),
            \implode(" AND ", $conditions)
            );
    }
    
    /**
     * return an array of classes where entities should be deleted 
     * instead of moved
     * 
     * @return array
     */
    protected function getDeleteEntities(): array
    {
        return [
            AccompanyingPeriod::class
        ];
    }
    
    /**
     * get the full table name with schema if it does exists
     */
    private function getTableName(ClassMetadata $metadata): string
    {
        return empty($metadata->getSchemaName()) ? 
                $metadata->getTableName() : 
                $metadata->getSchemaName().".".$metadata->getTableName();
    }

}
