<?php
/*
 * Copyright (C) 2016-2019 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Actions;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event triggered when an entity attached to a person is removed.
 * 
 *
 */
class ActionEvent extends Event
{
    const DELETE = 'CHILL_PERSON.DELETE_ASSOCIATED_ENTITY';
    const MOVE   = 'CHILL_PERSON.MOVE_ASSOCIATED_ENTITY';
    
    /**
     *
     * @var int
     */
    protected $personId;
    
    /**
     * the FQDN class name as recorded in doctrine
     *
     * @var string
     */
    protected $entity;
    
    /**
     * an array of key value data to describe the movement
     *
     * @var array
     */
    protected $metadata;
    
    /**
     * the sql statement
     *
     * @var string 
     */
    protected $sqlStatement;
    
    /**
     *
     * @var string[]
     */
    protected $preSql = [];
    
    /**
     *
     * @var string[]
     */
    protected $postSql = [];
    
    public function __construct($personId, $entity, $sqlStatement, $metadata = [])
    {
        $this->personId = $personId;
        $this->entity = $entity;
        $this->sqlStatement = $sqlStatement;
        $this->metadata = $metadata;
    }

    /**
     * 
     * @return string[]
     */
    public function getPreSql(): array
    {
        return $this->preSql;
    }

    /**
     * 
     * @return string[]
     */
    public function getPostSql(): array
    {
        return $this->postSql;
    }

    /*
     * Add Sql which will be executed **before** the delete statement
     */
    public function addPreSql(string $preSql)
    {
        $this->preSql[] = $preSql;
        return $this;
    }

    /**
     * Add Sql which will be executed **after** the delete statement
     * 
     * @param type $postSql
     * @return $this
     */
    public function addPostSql(string $postSql)
    {
        $this->postSql[] = $postSql;
        return $this;
    }

    public function getPersonId(): int
    {
        return $this->personId;
    }

    /**
     * get the entity name, as recorded in doctrine
     * 
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }
    
    public function getSqlStatement()
    {
        return $this->sqlStatement;
    }
    
    public function getMetadata()
    {
        return $this->metadata;
    }
    
}
