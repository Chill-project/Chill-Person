<?php
/*
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Search;

use Chill\MainBundle\Search\AbstractSearch;
use Chill\PersonBundle\Repository\PersonRepository;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Search\SearchInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Templating\EngineInterface;

/**
 * 
 *
 */
class PersonSearchByPhone extends AbstractSearch
{
    
    /**
     *
     * @var PersonRepository
     */
    private $personRepository;

    /**
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     *
     * @var AuthorizationHelper
     */
    private $helper;

    /**
     *
     * @var PaginatorFactory
     */
    protected $paginatorFactory;
    
    /**
     *
     * @var bool
     */
    protected $activeByDefault;
    
    /**
     *
     * @var Templating
     */
    protected $engine;
    
    const NAME = 'phone';
    
    public function __construct(
        PersonRepository $personRepository, 
        TokenStorageInterface $tokenStorage, 
        AuthorizationHelper $helper, 
        PaginatorFactory $paginatorFactory,
        EngineInterface $engine,
        $activeByDefault)
    {
        $this->personRepository = $personRepository;
        $this->tokenStorage = $tokenStorage;
        $this->helper = $helper;
        $this->paginatorFactory = $paginatorFactory;
        $this->engine = $engine;
        $this->activeByDefault = $activeByDefault === 'always';
    }
    
    public function getOrder(): int
    {
        return 110;
    }

    public function isActiveByDefault(): bool
    {
        return $this->activeByDefault;
    }

    public function renderResult(array $terms, $start = 0, $limit = 50, $options = array(), $format = 'html')
    {
        $phonenumber = $terms['_default'];
        $centers     = $this->helper->getReachableCenters($this->tokenStorage
            ->getToken()->getUser(), new Role(PersonVoter::SEE));
        $total = $this->personRepository
            ->countByPhone($phonenumber, $centers);
        $persons = $this->personRepository
            ->findByPhone($phonenumber, $centers, $start, $limit)
            ;
        $paginator = $this->paginatorFactory
            ->create($total);
        
        return $this->engine->render('ChillPersonBundle:Person:list_by_phonenumber.html.twig',
                    array(
                        'persons' => $persons,
                        'pattern' => $this->recomposePattern($terms, array(), $terms['_domain'] ?? self::NAME),
                        'phonenumber' => $phonenumber,
                        'total' => $total,
                        'start' => $start,
                        'search_name' => self::NAME,
                        'preview' => $options[SearchInterface::SEARCH_PREVIEW_OPTION],
                        'paginator' => $paginator
                    ));
    }

    public function supports($domain, $format): bool
    {
        return $domain === 'phone' && $format === 'html';
    }
}
