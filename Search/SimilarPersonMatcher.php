<?php
/*
 *
 * Copyright (C) 2018, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Search;

use Doctrine\ORM\EntityManagerInterface;
use Chill\PersonBundle\Entity\Person;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Security\Authorization\PersonVoter;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class SimilarPersonMatcher
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TokenStorageInterface 
     */
    protected $tokenStorage;
    
    public function __construct(
        EntityManagerInterface $em, 
        AuthorizationHelper $authorizationHelper, 
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
    }

    
    public function matchPerson(Person $person)
    {
        $centers = $this->authorizationHelper
            ->getReachableCenters(
                $this->tokenStorage->getToken()->getUser(), 
                new Role(PersonVoter::SEE)
                );
        
        $dql = 'SELECT p from ChillPersonBundle:Person p WHERE'
            . ' ('
            . '     UNACCENT(LOWER(p.firstName)) LIKE UNACCENT(LOWER(:firstName)) '
            . '     OR UNACCENT(LOWER(p.lastName)) LIKE UNACCENT(LOWER(:lastName)) '
            . '     OR UNACCENT(LOWER(p.firstName)) LIKE UNACCENT(LOWER(:lastName)) '
            . '     OR UNACCENT(LOWER(p.lastName)) LIKE UNACCENT(LOWER(:firstName)) '
            . '     OR SIMILARITY(p.fullnameCanonical, UNACCENT(LOWER(:fullName))) >= 0.15 '
            . ' ) '
            . ' AND p.center IN (:centers)'
            . ' ORDER BY SIMILARITY(p.fullnameCanonical, UNACCENT(LOWER(:fullName))) DESC '
            ;
        
        $query = 
            $this->em
            ->createQuery($dql)
            ->setParameter('firstName', $person->getFirstName())
            ->setParameter('lastName', $person->getLastName())
            ->setParameter('fullName', $person->getFirstName() . ' ' . $person->getLastName())
            ->setParameter('centers', $centers)
            ;
        
        return $query->getResult();
    }
}
