<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\PersonBundle\Tests\Export\Filter;

use Chill\MainBundle\Test\Export\AbstractFilterTest;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * 
 *
 */
class AccompanyingPeriodFilterTest extends AbstractFilterTest
{
    /**
     *
     * @var \Chill\PersonBundle\Export\Filter\BirthdateFilter
     */
    private $filter;
    
    public function setUp()
    {
        static::bootKernel();
        
        $container = static::$kernel->getContainer();
        
        try {
            $this->filter = $container->get('chill.person.export.filter_accompanying_period');
        } catch  (ServiceNotFoundException $e) {
            $this->markTestSkipped("The current configuration does not use accompanying_periods");
        }
    }
    
    
    public function getFilter()
    {
        return $this->filter;
    }

    public function getFormData()
    {
        return array(
            array(
                'date_from' => \DateTime::createFromFormat('Y-m-d', '2000-01-01'),
                'date_to'   => \DateTime::createFromFormat('Y-m-d', '2010-01-01')
            )
        );
    }

    public function getQueryBuilders()
    {
        if (static::$kernel === null) {
            static::bootKernel();
        }
        
        $em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
        
        return array(
            $em->createQueryBuilder()
                ->select('person.firstName')
                ->from('ChillPersonBundle:Person', 'person'),
            $em->createQueryBuilder()
                ->select('person.firstName')
                ->from('ChillPersonBundle:Person', 'person')
                // add a dummy where clause
                ->where('person.firstname IS NOT NULL'),
            $em->createQueryBuilder()
                ->select('count(IDENTITY(p))')
                ->from('ChillPersonBundle:Person', 'person')
                // add a dummy where clause
                ->where('person.firstname IS NOT NULL'),
            $em->createQueryBuilder()
                ->select('count(IDENTITY(p))')
                ->from('ChillPersonBundle:Person', 'person')
                ->join('person.accompanyingPeriods', 'accompanying_period')
                // add a dummy where clause
                ->where('person.firstname IS NOT NULL'),
            $em->createQueryBuilder()
                ->select('activity.date AS date')
                ->select('activity.attendee as attendee')
                ->from("ChillActivityBundle:Activity", 'activity')
                ->join('activity.person', 'person')
                ->join('person.center', 'center')
        );
    }
}
